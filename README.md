# Application [Formes Animées](https://lig-membres.imag.fr/genoud/teaching/PL2AI/tds/POO/sujets/tp11_chenillesEtFormes/tp11_chenillesEtFormes.html)

Ce projet contient le code source (sous la forme d'un projet maven d'application java) d'une solution
 du TP JAVA [Formes Animées](https://lig-membres.imag.fr/genoud/teaching/PL2AI/tds/POO/sujets/tp11_chenillesEtFormes/tp11_chenillesEtFormes.html)
 du cours Java du Master 2 Compétences Complémentaires en Informatique (M2CCI).

Pour récupérer les sources de ce projet vous pouvez

* soit utiliser git en effectuant l'une des commande de clonage du projet

  * via HTTPS
   ```
   git clone https://gricad-gitlab.univ-grenoble-alpes.fr/enseignement1/m2cci/java/formes-animees.git
   ```

  * via SSH
   ```
   git clone git@gricad-gitlab.univ-grenoble-alpes.fr:enseignement1/m2cci/java/formes-animees.git
   ```
   
* soit récupérer une archive (.tar.gz ou .zip) de l'une des [releases](https://gricad-gitlab.univ-grenoble-alpes.fr/enseignement1/m2cci/java/formes-animees/-/releases/) du projet


Le dépôt git permet de suivre l'évolution du programme **Formes Animées** exercice par exercice.
A chaque version est associée un tag (**v1**, **v2**, ... le numéro du tag indiquant numéro de l'exercice traité) qui vous permet de récupérer facilement le code correspondant à l'état de l'application à la fin de chaque exercice (un tag permet d'identifier facilement un commit donné).

La version v7 correspond à la correction de l'[exercice 7](https://lig-membres.imag.fr/genoud/teaching/PL2AI/tds/POO/sujets/tp11_chenillesEtFormes/tp11_chenillesEtFormes.html#section07) du TP : animation paramétrée des Chenilles.

La version v7.1 y ajoute un nouveau type de forme : les Disques

La version v7.2 permet d'avoir un meilleur packaging afin d'avoir un jar exécutable intégrant les images des têtes de chenilles *Star War*.

Pour avoir le détail des différentes versions utilisez la commande

```bash
git -tag -l -n
```

Pour récupérer le code source correspondant à une version donnée 

```bash
git checkout -b exercice<noversion> v<noversion>
```

qui permet de créer une branche de nom `exercice<noversion>` en récupérant le code de la version `v<noversion>`. Par exemple

```bash
git checkout -b exercice1 v1
```

vous permet de récupérer dans la branche `exercice1` le code de la version `v1`. **Attention** une fois cette commande effectuée, vous êtes positionné sur la branche `exercice1` (votre répertoire de travail (*working directory*) contient les fichiers correspondant au commit identifié par le tag `v1`). Vous pouvez revenir sur la branche `master` à l'aide de la commande

```
git checkout master
```

Vous pouvez alors (si vous le souhaitez) détruire la branche `exercice1` par la commande 

```bash
git branch -d exercice1
```

Vous pouvez aussi à l'aide de la commande `git diff` visualiser facilement les différences entre deux versions d'un code. Par exemple

```
git diff v2 v1  ./src/main/java/fr/im2ag/m2cci/chenilles/Dessin.java
```
vous montrera les différences entre la version `v2` et la version `v1` du code Java de la classe **Dessin**

## Les Différentes Releases
### **v1 : Animer Simultanément des Chenilles et des Visages ronds**

Correction de l'[exercice 1](http://lig-membres.imag.fr/genoud/teaching/PL2AI/tds/POO/sujets/tp11_chenillesEtFormes/tp11_chenillesEtFormes.html#section01) : 
animer des visages avec les chenilles.

### **v2 : Afficher des étoiles fixes**

Correction de l'[exercice 2](http://lig-membres.imag.fr/genoud/teaching/PL2AI/tds/POO/sujets/tp11_chenillesEtFormes/tp11_chenillesEtFormes.html#section02) : 
Afficher des étoiles fixes.
### **v3 : Afficher d'autres formes fixes: les polygones réguliers**

Correction de l'[exercice 3](http://lig-membres.imag.fr/genoud/teaching/PL2AI/tds/POO/sujets/tp11_chenillesEtFormes/tp11_chenillesEtFormes.html#section03) : 
Afficher d'autres formes fixes: les polygones réguliers.
       
### **v4.1 : Refactoring du code - organisation en packages**

Correction de l'[exercice 4.1](http://lig-membres.imag.fr/genoud/teaching/PL2AI/tds/POO/sujets/tp11_chenillesEtFormes/tp11_chenillesEtFormes.html#section04_1) : 
Réorganisation des classes en différents packages.

### **v4.2 : Refactoring du code - généralisation des formes**

Correction de l'[exercice 4.2](http://lig-membres.imag.fr/genoud/teaching/PL2AI/tds/POO/sujets/tp11_chenillesEtFormes/tp11_chenillesEtFormes.html#section04_2) : 
Généralisation des formes.

### **v5 : Animer les formes**

Correction de l'[exercice 5](http://lig-membres.imag.fr/genoud/teaching/PL2AI/tds/POO/sujets/tp11_chenillesEtFormes/tp11_chenillesEtFormes.html#section05) : 
animer les formes.
### **v6 : Animateur avec Cap aléatoire**

Correction de l'[exercice 6](http://lig-membres.imag.fr/genoud/teaching/PL2AI/tds/POO/sujets/tp11_chenillesEtFormes/tp11_chenillesEtFormes.html#section06) : 
un nouveau type d'animateur, l'animateur avec cap aléatoire.
### **v7 : Animation paramétrée des Chenilles**

Correction de l'[exercice 7](http://lig-membres.imag.fr/genoud/teaching/PL2AI/tds/POO/sujets/tp11_chenillesEtFormes/tp11_chenillesEtFormes.html#section07) : 
permettre différents comportements d'animation pour les Chenilles.

### **v7.1 : Ajout d'un nouveau type de formes, les Disques**

Ajout d'un nouveau type de forme, les disques

### **v7.2 : Améliorations diverses**

* Les images des têtes *Star Wars* sont stockées dans les ressources du projet afin de pouvoir les intégrer au **. jar** crée par la commande <kbd>mvn package</kbd>

* Le fichier **pom.xml** a été modifié 
    * pour pouvoir générer un *fat jar* intégrant les librairies des dépendances (ici **jopt-simple**)
    * pour rendre ce jar exécutable en fixant la classe contenant le main a exécuter (ici **AppliNChenilles**).

* Installation de maven wrapper pour faciliter l'utilisation de maven