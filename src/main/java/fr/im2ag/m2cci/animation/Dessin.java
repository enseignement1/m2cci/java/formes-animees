/*
 * Copyright (C) 2022 Philippe GENOUD - Université Grenoble Alpes (UGA) - Laboratoire Informatique de Grenoble (LIG) - équipe STeamer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.im2ag.m2cci.animation;

/**
 * Dessin qui contient des objets IAnimables
 * @author Philippe GENOUD - Université Grenoble Alpes (UGA) - Laboratoire
 * Informatique de Grenoble (LIG) - équipe STeamer
 */
import java.awt.Graphics;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import javax.swing.JPanel;

/**
 * Defini le contenu de la fenêtre de l'application d'animation.
 * Une zone de dessin est un JPanel qui gère un liste d'objets IDessinables.
 * Lorsqu'il se réaffiche l'objet Dessin redessinne les différents objets
 * IDessinables contenus dans cette liste.
 *
 * @author Philippe Genoud
 */
public class Dessin extends JPanel {

    /**
     * stocke la liste des IDessinables ayant été ajoutées à cette zone de dessin.
     */
    private final List<IDessinable> listeDesObjets = new CopyOnWriteArrayList<>();

    /**
     * retourne la largeur de la zone de dessin.
     *
     * @return la largeur.
     */
    public int getLargeur() {
        return getWidth();
    }

    /**
     * retourne la hauteur de la zone de dessin.
     *
     * @return la hauteur.
     */
    public int getHauteur() {
        return getHeight();
    }

    /**
     * ajoute un IDessinable à la zone de dessin.
     *
     * @param o l'objet IDessinable à ajouter au Dessin
     */
    public void ajouterObjet(IDessinable o) {

        if (!listeDesObjets.contains(o)) {
            // l'objet n'est pas déjà dans la liste
            // on le rajoute à la liste des objets du dessin
            listeDesObjets.add(o);
            // le dessin se réaffiche
            repaint();
            this.pause(10);
        }
    }

    /**
     * temporisation de l'animation.
     * @param duree delai de temporisation en ms.
     */
    public void pause(int duree) {
        try {
            Thread.sleep(duree);
        } catch (Exception e) {
        }
    }

    /**
     * affiche la zone de dessin et son contenu
     *
     * @param g le contexte graphique
     */
    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        // on dessine chacun des visages contenus dans la zone de dessin
        for (IDessinable o : listeDesObjets) {
            o.dessiner(g);
        }
    }

    /**
     * fait faire un déplacement élémentaire à toutes les objets IAnimables
     * situés dans la zone de dessin
     */
    public void deplacerObjets() {
        for (IDessinable o : listeDesObjets) {
            if (o instanceof IAnimable) {
                IAnimable a = (IAnimable) o;
                a.deplacer();
                // plutôt que les deux instructions précédentes, on aurait pu écrire directement
                // ((IAnimable) o).deplacer();
            }
            
        }
    }

} // Dessin

