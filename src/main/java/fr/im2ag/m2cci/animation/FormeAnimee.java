/*
 * Copyright (C) 2021 Philippe GENOUD - Université Grenoble Alpes (UGA) - Laboratoire Informatique de Grenoble (LIG) - équipe STeamer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.im2ag.m2cci.animation;

import java.awt.Graphics;
import java.awt.Rectangle;

/**
 * Une forme annimée est un objet animable, c'est l'association d'une forme
 * et d'un animateur chargé de lui faire effectuer ses déplacments élementaires.
 *
 * @author Philippe GENOUD - Université Grenoble Alpes (UGA) - Laboratoire
 * Informatique de Grenoble (LIG) - équipe STeamer
 */
public class FormeAnimee implements IFormeAnimee {

    /**
     * la forme
     */
    private final IForme forme;
    
    /**
     * l'animateur en charge des déplacement élementatires de la forme
     */
    private final IAnimateur animateur;

    /**
     * Constructeur
     * @param forme la forme à animer
     * @param animateur l'animateur associé
     */
    public FormeAnimee(IForme forme, IAnimateur animateur) {
        this.forme = forme;
        this.animateur = animateur;
    }

    // implémentation des méthodes de l'interface IAnimable
    
    @Override
    public void deplacer() {
        this.animateur.animer(this.forme);
    }

    @Override
    public void dessiner(Graphics g) {
        this.forme.dessiner(g);
    }

    @Override
    public Rectangle getRectEnglobant() {
        return forme.getRectEnglobant();
    }

    @Override
    public void placerA(int x, int y) {
        forme.placerA(x, y);
        
    }

    @Override
    public int getX() {
        return forme.getX();
    }

    @Override
    public int getY() {
        return forme.getY();
    }
}

