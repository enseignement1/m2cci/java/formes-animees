/*
 * Copyright (C) 2022 Philippe GENOUD - Université Grenoble Alpes (UGA) - Laboratoire Informatique de Grenoble (LIG) - équipe STeamer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.im2ag.m2cci.animation.animateurs;

import java.awt.Rectangle;

import fr.im2ag.m2cci.animation.Dessin;
import fr.im2ag.m2cci.animation.IAnimateur;

/**
 * Classe d'animateurs qui ont besoin de connaître le Dessin pour animer la 
 * forme dont ils ont la charge. Ils permettent de détecter si la forme
 * sort de la zone de dessin.
 * 
 * @author Philippe GENOUD - Université Grenoble Alpes (UGA) - Laboratoire
 * Informatique de Grenoble (LIG) - équipe STeamer
 */
public abstract class AnimateurAvecDessin implements IAnimateur {

    /**
     * le dessin dans lequel l'animation a lieu
     */
    protected final Dessin d;

    /**
     * constructeur
     * @param d le dessin dans lequel l'animation a lieu
     */
    public AnimateurAvecDessin(Dessin d) {
        this.d = d;
    }

       // methodes de test de la forme par rapport à la zone de dessin
    /**
     * teste si la forme sort sur la gauche de la zone de dessin
     *
     * @param r le rectangle englobant de forme à animer
     * @return true si le rectangle englobant de la forme sort à gauche de la
     * zone de dessin, false sinon
     */
    protected boolean sortAGauche(Rectangle r) {
        return r.getX() <= 0;
    }

    /**
     * teste si la forme sort sur la droite de la zone de dessin
     *
     * @param r le rectangle englobant de forme à animer
     * @return true si le rectangle englobant de la forme sort à droite de la
     * zone de dessin, false sinon
     */
    protected boolean sortADroite(Rectangle r) {
        return r.getX() + r.getWidth() >= d.getLargeur();
    }

    /**
     * teste si la forme sort en haut de la zone de dessin
     *
     * @param r le rectangle englobant de forme à animer
     * @return true si le rectangle englobant de la forme sort sur le haut de la
     * zone de dessin, false sinon
     */
    protected boolean sortEnHaut(Rectangle r) {
        return r.getY() <= 0;
    }

    /**
     * teste si la forme sort en bas de la zone de dessin
     *
     * @param r le rectangle englobant de forme à animer
     * @return true si le rectangle englobant de la forme sort sur le bas de la
     * zone de dessin, false sinon
     */
    protected boolean sortEnBas(Rectangle r) {
        return r.getY() + r.getHeight() >= d.getHauteur();
    }
}

