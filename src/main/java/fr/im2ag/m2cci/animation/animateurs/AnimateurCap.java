/*
 * Copyright (C) 2022 Philippe GENOUD - Université Grenoble Alpes (UGA) - Laboratoire Informatique de Grenoble (LIG) - équipe STeamer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.im2ag.m2cci.animation.animateurs;

import java.awt.Rectangle;

import fr.im2ag.m2cci.animation.Dessin;
import fr.im2ag.m2cci.animation.IForme;

/**
 * Animateur d'une forme avec déplacement selon un cap aléatoire
 * 
 * @author Philippe GENOUD - Université Grenoble Alpes (UGA) - Laboratoire
 * Informatique de Grenoble (LIG) - équipe STeamer
 */
public class AnimateurCap extends AnimateurAvecDessin {

    /**
     * le cap (exprimé en degrés)
     */
    private double cap;

    /**
     * la distance de déplacement élémentaire dans la direction du cap
     */
    private final int deplacementElem;

    /**
     *
     * @param d
     * @param depElem la distance de déplacement élémentaire
     * @param cap le cap initial
     */
    public AnimateurCap(Dessin d, int depElem, double cap) {
        super(d);
        this.cap = cap;
        this.deplacementElem = depElem;
    }

    /**
     * normalize un angle pour le ramener à un valeur dans l'intervalle [-180°,
     * +180°]
     *
     * @param d la valeur de l'angle en degrés
     * @return langle normalisé
     */
    private double normalize(double d) {
        double res = Math.abs(d) % 360;
        if (d < 0) {
            if (res > 180) {
                res = 360 - res;
            } else {
                res = -res;
            }
        } else {
            if (res > 180) {
                res = -(360 - res);
            }
        }
        return res;
    }

    /**
     * modifie le cap
     *
     * @param deltaC la valeur à ajouter au cap
     */
    private void devierCap(double deltaC) {
        cap += deltaC;
        this.cap = normalize(cap);
    }

    /**
     * modifie le point de référence de la forme de manière à ce que celui-ci
     * soit translaté d'une distance définie par deplacementElem dans la
     * direction du cap.
     *
     * @param f la forme à déplacer
     */
    private void deplacerSelonCap(IForme f) {
        f.placerA((int) (f.getX() + this.deplacementElem * Math.cos(Math.PI * cap / 180)),
                (int) (f.getY() + this.deplacementElem * Math.sin(Math.PI * cap / 180)));
    }

    /**
     * teste si le cap actuel garantit que prochain déplacement de la forme
     * selon son cap maintiendra celle-ci entièrement dans la zone de dessin.
     *
     * @param f la forme à deplacer
     *
     * @return true si la boite englobante après une translation de
     * deplacementElem dans la direction du cap est entièrement dans la zone de
     * dessin
     */
    private boolean capOK(IForme f) {
        Rectangle r = new Rectangle(f.getRectEnglobant());
        r.translate((int) (deplacementElem * Math.cos(Math.toRadians(cap))),
                (int) (deplacementElem * Math.sin(Math.toRadians(cap))));
        return !sortAGauche(r) && !sortADroite(r) && !sortEnHaut(r) && !sortEnBas(r);
    }

    @Override
    public void animer(IForme f) {
        // calcule un nouveau cap qui garanti que la forme reste dans la zone
        // de dessin
        this.devierCap(-30.0 + Math.random() * 60.0);
        while (! this.capOK(f)) {
            this.devierCap(10);
        }
        // fait avancer la forme
        this.deplacerSelonCap(f);
    }

}

