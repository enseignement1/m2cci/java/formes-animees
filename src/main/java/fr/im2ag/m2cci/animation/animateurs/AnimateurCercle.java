/*
 * Copyright (C) 2022 Philippe GENOUD - Université Grenoble Alpes (UGA) 
 * - Laboratoire Informatique de Grenoble (LIG) - équipe STeamer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.im2ag.m2cci.animation.animateurs;

import fr.im2ag.m2cci.animation.IAnimateur;
import fr.im2ag.m2cci.animation.IForme;

/**
 * Animateur permettant de déplacer une forme avec un mouvement circulaire
 * uniforme.
 *
 * @author Philippe GENOUD - Université Grenoble Alpes (UGA) - Laboratoire
 *                 Informatique de Grenoble (LIG) - équipe STeamer
 */
public class AnimateurCercle implements IAnimateur {

    /**
     * rayon du cercle sur lequel l'animateur déplace la forme
     */
    private final int rayon;

    /**
     * abscisse du centre du cercle sur lequel l'animateur déplace la forme
     */
    private final int xc;

    /**
     * ordonnée du centre du cercle sur lequel l'animateur déplace la forme
     */
    private final int yc;

    /**
     * angle où se situe le point de référence de la forme animée.
     */
    private double angle;

    /**
     * variation de l'angle de rotation à chaque pas d'animation
     */
    private final double deltaAngle;

    /**
     * Constructeur
     *
     * @param xc abscisse du centre du cercle sur lequel l'animateur déplace la
     * forme
     * @param yc ordonnée du centre du cercle sur lequel l'animateur déplace la
     * forme
     * @param r rayon du cercle sur lequel l'animateur déplace la forme
     * @param angle angle initial où se situe le point de référence de la forme
     * animée.
     * @param deltaAngle variation de l'angle de rotation à chaque pas
     * d'animation
     */
    public AnimateurCercle(int xc, int yc, int r, double angle, double deltaAngle) {
        this.deltaAngle = deltaAngle;
        this.angle = angle;
        this.rayon = r;
        this.xc = xc;
        this.yc = yc;
    }

    // implémentation de l'interface IAnimateur
    @Override
    public void animer(IForme f) {
        angle += deltaAngle;
        double angleRadians = Math.toRadians(angle);
        f.placerA((int) (xc + rayon * Math.cos(angleRadians)),
                (int) (yc + rayon * Math.sin(angleRadians)));
    }

}
