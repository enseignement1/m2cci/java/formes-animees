/*
 * Copyright (C) 2022 Philippe GENOUD - Université Grenoble Alpes (UGA) - Laboratoire Informatique de Grenoble (LIG) - équipe STeamer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.im2ag.m2cci.animation.animateurs;

import java.awt.Rectangle;

import fr.im2ag.m2cci.animation.Dessin;
import fr.im2ag.m2cci.animation.IForme;

/**
 *
 * @author Philippe GENOUD - Université Grenoble Alpes (UGA) - Laboratoire
 * Informatique de Grenoble (LIG) - équipe STeamer
 */
public class AnimateurRebond extends AnimateurAvecDessin {

    /**
     * déplacement élémentaire horizontal
     */
    private int dx;

    /**
     * déplacement élémentaire vertical
     */
    private int dy;

    public AnimateurRebond(Dessin d, int dx, int dy) {
        super(d);
        this.dx = dx;
        this.dy = dy;
    }

    
    // implementation de l'interface IAnimable
    @Override
    public void animer(IForme f) {
        
        Rectangle r = f.getRectEnglobant();
        if (sortAGauche(r) || sortADroite(r)) {
            this.dx = -this.dx;
        }
        if (sortEnHaut(r) || sortEnBas(r)) {
            this.dy = -this.dy;
        }
        f.placerA(f.getX() + dx, f.getY() + dy);
    }
}

