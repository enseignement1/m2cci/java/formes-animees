/*
 * Copyright (C) 2022 Philippe GENOUD - Université Grenoble Alpes (UGA) - Laboratoire Informatique de Grenoble (LIG) - équipe STeamer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.im2ag.m2cci.animation.applis;

import java.awt.Color;
import javax.swing.JFrame;
import javax.swing.WindowConstants;

import fr.im2ag.m2cci.animation.Dessin;
import fr.im2ag.m2cci.animation.chenilles.ChenilleCouleur;

/**
 * Affiche deux chenilles animées dans une fenêtre d'application
 * 
 * @author Philippe GENOUD - Université Grenoble Alpes (UGA) - Laboratoire
 * Informatique de Grenoble (LIG) - équipe STeamer
 */
public class AppliChenillesCouleur {

    public static void main(String[] args) {

        // la fenêtre graphique
        JFrame laFenetre = new JFrame("CHENILLES ANIMES");
        laFenetre.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        laFenetre.setSize(800, 550);

        // créé la zone de dessin et la place dans la fenêtre
        Dessin d = new Dessin();
        laFenetre.add(d);

        //  affiche la fenêtre
        laFenetre.setVisible(true);

        d.ajouterObjet(new ChenilleCouleur(d, 10, 14, new Color(0,0, 255)));
        d.ajouterObjet(new ChenilleCouleur(d, 10, 14, new Color(0,255, 0)));

        // la boucle d'animation
        // c'est une boucle infinie, le programme devra être interrompu
        // par CTRL-C ou en cliquant dans la case de fermeture de la fenêtre
        while (true) {
            // les chenilles effectuent un déplacement élémentaire
            // en rebondissant sur les bords de la zone de dessin
            d.deplacerObjets();
            // efface la zone de dessin et la réaffiche
            d.repaint();

            // un temps de pause pour avoir le temps de voir le nouveau dessin
            d.pause(30);
        }
    }
}
