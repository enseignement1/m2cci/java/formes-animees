/*
 * Copyright (C) 2022 Philippe GENOUD - Université Grenoble Alpes (UGA) - Laboratoire Informatique de Grenoble (LIG) - équipe STeamer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.im2ag.m2cci.animation.applis;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Arrays;

import javax.imageio.ImageIO;
import javax.swing.JFrame;
import javax.swing.WindowConstants;

import fr.im2ag.m2cci.animation.Dessin;
import fr.im2ag.m2cci.animation.FormeAnimee;
import fr.im2ag.m2cci.animation.animateurs.AnimateurCap;
import fr.im2ag.m2cci.animation.animateurs.AnimateurCercle;
import fr.im2ag.m2cci.animation.animateurs.AnimateurRebond;
import fr.im2ag.m2cci.animation.chenilles.Chenille;
import fr.im2ag.m2cci.animation.chenilles.ChenilleCouleur;
import fr.im2ag.m2cci.animation.chenilles.ChenilleImage;
import fr.im2ag.m2cci.animation.formes.Etoile;
import fr.im2ag.m2cci.animation.formes.PolygoneRegulier;
import fr.im2ag.m2cci.animation.visages.VisageRond;
import joptsimple.OptionParser;
import joptsimple.OptionSet;

import static fr.im2ag.m2cci.utils.FileUtils.getFileFromResourceAsStream;

/**
 * Affiche n chenilles animées dans une fenêtre d'application, le nombre de
 * chenilles étant fixé à l'aide des paramètres de la ligne de commandes.
 * 
 * Les différentes options de la ligne de commande sont
 * 
 * - sw : definit le nombre de chenilles "starWar", 4 par défaut
 * 
 * - coul : definit le nombre de chenilles couleur, 3 par défaut
 * 
 * - normal : definit le nmbre de chenilles normales, 2 par défaut
 * 
 * - delay : le temps de pause (en ms) entre deux étapes d'animation, 80ms par
 * défaut.
 * 
 * Par exemple l'appel de AppliNChenilles avec les arguments
 * 
 * --sw = 5 --coul=3 --normal=2 --delay=60
 * 
 * affichera 5 chenilles StarWar, 3 cheniles couleur, 2 chenilles normales avec
 * un temps de pause de 60 ms
 * entre chaque réaffichage
 *
 * @author Philippe GENOUD - Université Grenoble Alpes (UGA) - Laboratoire
 *         Informatique de Grenoble (LIG) - équipe STeamer
 */
public class AppliNChenilles {

    // les chemins d'accès (relatifs) aux fichiers images
    public static final String[] tetesFileNames = {
            "images/darthVador.png",
            "images/leila.png",
            "images/c3po.png",
            "images/stormTrooper.png"
    };

    public static final Color[] COULEURS = { Color.BLUE, Color.RED, Color.green, Color.MAGENTA, Color.CYAN };

    public static void main(String[] args) throws IOException {

        // récupération des arguments de la ligne de commande
        // on utilsie la librairie Jopt-Simple (voir
        // https://github.com/jopt-simple/jopt-simple)
        OptionParser parser = new OptionParser();
        parser.accepts("sw", "count of SW caterpillars").withRequiredArg()
                .ofType(Integer.class).defaultsTo(4);
        parser.accepts("coul", "count of colored caterpillars").withRequiredArg()
                .ofType(Integer.class).defaultsTo(3);
        parser.accepts("normal", "count of normal (Black) caterpillars").withRequiredArg()
                .ofType(Integer.class).defaultsTo(2);
        parser.accepts("delay", "delay (in ms) between two animation frames").withRequiredArg()
                .ofType(Integer.class).defaultsTo(80);
        parser.acceptsAll(Arrays.asList("h", "?"), "show help").forHelp();

        OptionSet options = parser.parse(args);
        if (options.has("h") || options.has("?")) {
            parser.printHelpOn(System.out);
            System.exit(0);
        }

        int nbChenillesSW = (int) options.valueOf("sw"); // le nombre de Chenilles 'normales' à créer
        int nbChenillesCoul = (int) options.valueOf("coul"); // le nombre de Chenilles 'couleur' à créer
        int nbChenilles = (int) options.valueOf("coul"); // le nombre de Chenilles 'normales' à créer
        int tempsPause = (int) options.valueOf("delay"); // le temps de pause entre deux réaffichages

        // récupération des images pour les cheniles StarWar
        BufferedImage[] images = new BufferedImage[tetesFileNames.length];
        for (int i = 0; i < images.length; i++) {
            images[i] = ImageIO.read(getFileFromResourceAsStream(tetesFileNames[i]));
        }

        // création de la fenêtre graphique
        JFrame laFenetre = new JFrame(
                "CHENILLES ANIMES");
        laFenetre.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        laFenetre.setSize(800, 550);

        // créé la zone de dessin et la place dans la fenêtre
        Dessin d = new Dessin();
        laFenetre.add(d);

        // affiche la fenêtre
        laFenetre.setVisible(true);

        // création des Chenilles
        for (int i = 0; i < nbChenillesSW; i++) {
            d.ajouterObjet(new ChenilleImage(d, 12, 15, (i < 3) ? images[i] : images[3]));
        }
        for (int i = 0; i < nbChenillesCoul; i++) {
            int noCouleur = (int) Math.floor(Math.random() * (COULEURS.length));
            d.ajouterObjet(new ChenilleCouleur(d, 10, 14, COULEURS[noCouleur]));
        }
        for (int i = 0; i < nbChenilles; i++) {
            d.ajouterObjet(new Chenille(d, 10, 14));
        }

        d.ajouterObjet(new ChenilleImage(d, 12, 15, images[3], new AnimateurRebond(d, 12, 12)));
        d.ajouterObjet(new ChenilleCouleur(d, 10, 14, Color.YELLOW, new AnimateurCap(d, 14, -10)));
        d.ajouterObjet(new Chenille(d, 10, 14, new AnimateurCercle(400, 250, 180, 0, 5)));
        // création des visages
        d.ajouterObjet(new VisageRond(d, 100, 200, 50, 50));
        d.ajouterObjet(new VisageRond(d, 300, 100, 40, 40, -4, 4));

        // création et ajout de deux étoiles au dessin
        d.ajouterObjet(new Etoile(300, 200, 100, 2.0f, Color.RED, Color.YELLOW));
        d.ajouterObjet(new Etoile(100, 300, 50, 2.0f, Color.GREEN, Color.BLUE));

        // création et ajout au dessin de deux polygones réguliers
        // un pentagone rouge et un octogone vert
        d.ajouterObjet(new PolygoneRegulier(5, 600, 300, 50, 2.0f, Color.CYAN, Color.RED));
        d.ajouterObjet(new PolygoneRegulier(8, 500, 400, 30, 2.0f, Color.RED, Color.GREEN));

        // création de formes animées
        // une étoile jaune animée d'un mouvement circulaire
        d.ajouterObjet(new FormeAnimee(
                new Etoile(300, 200, 100, 7.0f, Color.RED, Color.YELLOW),
                new AnimateurCercle(500, 200, 200, 0.0, 5)));
        // un pentagone rouge au déplacement rectiligne et qui rebondit sur les bords de
        // la zone de dessin
        d.ajouterObjet(new FormeAnimee(
                new PolygoneRegulier(5, 600, 300, 50, 8.0f, Color.CYAN, Color.RED),
                new AnimateurRebond(d, 4, 4)));
        // une étoile bleue qui se déplace de manière aléatoire
        d.ajouterObjet(new FormeAnimee(
                new Etoile(100, 300, 50, 7.0f, Color.GREEN, Color.BLUE),
                new AnimateurCap(d, 5, 0)));

        // la boucle d'animation
        // c'est une boucle infinie, le programme devra être interrompu
        // par CTRL-C ou en cliquant dans la case de fermeture de la fenêtre
        while (true) {

            // efface la zone de dessin et la réaffiche
            d.repaint();

            // un temps de pause pour avoir le temps de voir le nouveau dessin
            d.pause(tempsPause);

            // fait faire un déplacement élémentaire à tous les objets de la zone de dessin
            d.deplacerObjets();
        }
    }
}
