/*
 * Copyright (C) 2022 Philippe GENOUD - Université Grenoble Alpes (UGA) - Laboratoire Informatique de Grenoble (LIG) - équipe STeamer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.im2ag.m2cci.animation.chenilles;

import java.awt.Graphics;
import java.awt.Rectangle;

import fr.im2ag.m2cci.animation.IForme;

/**
 * Représente un anneau d'une chenille.
 *
 * Un anneau est définit par :
 * <ul>
 * <li>les coordonnées x et y de son centre</li>
 * <li>son rayon</li>
 * </ul>
 *
 * @author Philippe GENOUD - Université Grenoble Alpes (UGA) - Laboratoire
 * Informatique de Grenoble (LIG) - équipe STeamer
 */
public class Anneau implements IForme {

    /**
     * rayon par défaut
     */
    public static final int DEFAULT_R = 10;

    //------- variables d'instance (attributs) --------------------------
    /**
     * abscisse du centre de l'anneau
     */
    protected int x;
    /**
     * ordonnée du centre de l'anneau
     */
    protected int y;
    /**
     * le rayon de l'anneau
     */
    protected int r;

    //-------- Constructeurs ---------------------------------------------
    /**
     * crée un Anneau en fixant la position initiale de son centre et son rayon
     *
     * @param xInit abscisse initiale du centre de l'anneau
     * @param yInit ordonnée initiale du centre de l'anneau
     * @param r rayon de l'anneau
     */
    public Anneau(int xInit, int yInit, int r) {
        this.x = xInit;
        this.y = yInit;
        this.r = r;
    }

    /**
     * crée un Anneau en fixant sa position initiale, son rayon étant le rayon
     * par défaut définit par DEFAULT_R
     *
     * @param xInit abscisse initiale du centre de l'anneau
     * @param yInit ordonnée initiale du centre de l'anneau
     */
    public Anneau(int xInit, int yInit) {
        this(xInit, yInit, DEFAULT_R);
    }

    //-------- Méthodes --------------------------------------------- 
    
    /**
     * retourne l'abscisse du centre de l'anneau
     *
     * @return abscisse du centre
     */
    public int getX() {
        return x;
    }

    /**
     * retourne ordonnée du centre de l'anneau
     *
     * @return ordonnée du centre
     */
    public int getY() {
        return y;
    }

    /**
     * retourne le rayon de l'anneau
     *
     * @return rayon de l'anneau
     */
    public int getR() {
        return r;
    }

    /**
     * positionne le centre de l'anneau en un point donné
     *
     * @param px abscisse du point
     * @param py ordonnée du point
     */
    public void placerA(int px, int py) {
        this.x = px;
        this.y = py;
    }

    /**
     * positionne l'anneau à la place d'un autre anneau
     *
     * @param a l'anneau dont le centre devient le centre de cet anneau (this)
     */
    public void placerA(Anneau a) {
        this.x = a.x;
        this.y = a.y;
    }

    /**
     * affiche l'anneau en le matérialisant par un cercle noir
     *
     * @param g objet de classe java.awt.Graphics qui prend en charge la gestion
     * de l'affichage dans la fenêtre de dessin. C'est cet objet qui gère le
     * "contexte graphique" pour cette fenêtre.
     */
    @Override
    public void dessiner(Graphics g) {
        g.drawOval(this.x - this.r, this.y - this.r, 2 * this.r, 2 * this.r);

    }

    @Override
    public Rectangle getRectEnglobant() {
        return new Rectangle(this.x - this.r, this.y - this.r, 2 * this.r, 2 * this.r);
    }

}
