/*
 * Copyright (C) 2022 Philippe GENOUD - Université Grenoble Alpes (UGA) - Laboratoire Informatique de Grenoble (LIG) - équipe STeamer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.im2ag.m2cci.animation.chenilles;

import java.awt.Graphics;

import fr.im2ag.m2cci.animation.Dessin;
import fr.im2ag.m2cci.animation.IAnimable;
import fr.im2ag.m2cci.animation.IAnimateur;
import fr.im2ag.m2cci.animation.animateurs.AnimateurCap;

/**
 * Représente une chenille animée
 *
 * @author Philippe GENOUD - Université Grenoble Alpes (UGA) - Laboratoire
 *         Informatique de Grenoble (LIG) - équipe STeamer
 */
public class Chenille implements IAnimable {

    /**
     * la tête de la chenille
     */
    protected Tete tete;

    /**
     * le corps de la chenille, une suite d'anneaux
     */
    private Anneau[] corps;

    /**
     * l'animateur en charge des mouvements de la tête
     */
    protected IAnimateur animateur;

    /**
     * Crée un chenille, sa tête est positionnée au centre de la zone de dessin,
     * ses anneaux sont situés à gauche de la tête et répartis de manière
     * horizontale Le centre de l'anneau i (i=1,2,...,n) est situé sur la
     * circonférence de l'anneau i−1 , le centre du premier anneau (anneau 0)
     * étant situé sur la circonférence de la tête.
     *
     * Ce constructeur a une visibilité protected. Il n'est pas public, car sa
     * vocation n'est pas d'être invoqué par un programme applicatif. Il est
     * prévu pour un usage interne, en particulier pour d'éventuelles sous
     * classes de Chenilles, afin de pouvoir créer des chenilles avec des types
     * de têtes différents.
     *
     * @param nbAnneaux le nombre d'anneaux de la chenille
     * @param rayon     le rayon des anneaux et de la tête de la chenille
     * @param tete      la tête de la chenille
     * @param animateur l'animateur en charge des déplacements de la tête de la
     *                  chenille
     */
    protected Chenille(int nbAnneaux, int rayon, Tete tete, IAnimateur animateur) {

        // création de la tête au centre de la zone de dessin
        this.tete = tete;
        // création du corps de la chenille
        // 1. création du tableau
        corps = new Anneau[nbAnneaux];
        // 2. remplissage du tableau en créant les anneaux et en stockant
        // leur référence dans les éléments du tableau. Les anneaux sont
        // positionnés horizontalement à gauche les uns des autres. Le premier
        // anneau (Anneau n° 0) étant à gauche de la tête
        for (int i = 0; i < corps.length; i++) {
            // création du ième anneau
            corps[i] = new Anneau(tete.getX() - (i + 1) * tete.getR(), tete.getY(), rayon);
        }
        this.animateur = animateur;
    }

    /**
     * Crée un chenille, sa tête est positionnée au centre de la zone de dessin,
     * ses anneaux sont situés à gauche de la tête et répartis de manière
     * horizontale Le centre de l'anneau i (i=1,2,...,n) est situé sur la
     * circonférence de l'anneau i−1 , le centre du premier anneau (anneau 0)
     * étant situé sur la circonférence de la tête.
     * 
     * @param dess      la zone de dessin dans laquelle la chenille sera affichée
     * @param nbAnneaux le nombre d'anneaux de la chenille
     * @param rayon     le rayon des anneaux et de la tête de la chenille
     * @param animateur l'animateur en charge des déplacements de la tête de la
     *                  chenille
     */
    public Chenille(Dessin dess, int nbAnneaux, int rayon, IAnimateur animateur) {
        this(nbAnneaux, rayon,
                new Tete(0.0, dess.getLargeur() / 2, dess.getHauteur() / 2, rayon),
                animateur);
    }

    /**
     * Crée un chenille, sa tête est positionnée au centre de la zone de dessin,
     * ses anneaux sont situés à gauche de la tête et répartis de manière
     * horizontale Le centre de l'anneau i (i=1,2,...,n) est situé sur la
     * circonférence de l'anneau i−1 , le centre du premier anneau (anneau 0)
     * étant situé sur la circonférence de la tête. L'animateur de la chenille
     * est par défaut un animateur de type AnimateurCap
     *
     * @param dess      la zone de dessin dans laquelle la chenille sera affichée
     * @param nbAnneaux le nombre d'anneaux de la chenille
     * @param rayon     le rayon des anneaux et de la tête de la chenille
     */
    public Chenille(Dessin dess, int nbAnneaux, int rayon) {
        this(dess, nbAnneaux, rayon, new AnimateurCap(dess, rayon, 0));
    }

    /**
     * dessine la chenille dans sa zone de dessin
     *
     * @param g cet objet de classe Graphics passé en paramètre est l'objet qui
     *          prend en charge la gestion de l'affichage dans la fenêtre de dessin.
     *          C'est cet objet qui gère le "contexte graphique" pour cette fenêtre.
     */
    @Override
    public void dessiner(Graphics g) {
        // dessiner la tête
        tete.dessiner(g);
        // dessiner le corps
        for (Anneau a : corps) {
            a.dessiner(g);
        }
    }

    /**
     * fait effectuer à la chenille un déplacement élémentaire dans la direction
     * indiquée par son cap. Le cap subit une déviation aléatoire d'un angle de
     * plus ou moins 30 degrés. Si la tête de la chenille atteint un des bords ,
     * le cap est modifié de manière à ce qu'elle rebondisse (c'est à dire à ce
     * que sa tête reste entièrement dans la zone de dessin)
     */
    @Override
    public void deplacer() {
        // déplacer les anneaux l'anneau i prend la palce de l'anneau i-1
        for (int i = corps.length - 1; i > 0; i--) {
            corps[i].placerA(corps[i - 1]);
        }
        // l'anneau 0 prend la place de la tête
        corps[0].placerA(tete);

        // l'animateur se charge de déplacer la tête
        this.animateur.animer(tete);

    }

}
