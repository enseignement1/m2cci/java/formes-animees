/*
 * Copyright (C) 2022 Philippe GENOUD - Université Grenoble Alpes (UGA) - Laboratoire Informatique de Grenoble (LIG) - équipe STeamer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.im2ag.m2cci.animation.chenilles;

import java.awt.Color;
import java.awt.Graphics;

import fr.im2ag.m2cci.animation.Dessin;
import fr.im2ag.m2cci.animation.IAnimateur;

/**
 * Chenille avec une couleur spécifique
 * 
 * @author Philippe GENOUD - Université Grenoble Alpes (UGA) - Laboratoire
 *         Informatique de Grenoble (LIG) - équipe STeamer
 */
public class ChenilleCouleur extends Chenille {

    /**
     * La couleur de la Chenille
     */
    private Color couleur;

    /**
     * Chenille colorée, l'animateur de la chenille est par défaut un animateur
     * de type AnimateurCap
     * 
     * @param dess      la zone de dessin dans laquelle la chenille sera affichée
     * @param nbAnneaux le nombre d'anneaux de la chenille
     * @param rayon     le rayon des anneaux et de la tête de la chenille
     * @param couleur   la couleur de dessin de la chenille
     */
    public ChenilleCouleur(Dessin dess, int nbAnneaux, int rayon, Color couleur) {
        super(dess, nbAnneaux, rayon);
        this.couleur = couleur;
    }

    /**
     * Chenille colorée, l'animateur de la chenille est par défaut un animateur
     * de type AnimateurCap
     * 
     * @param dess      la zone de dessin dans laquelle la chenille sera affichée
     * @param nbAnneaux le nombre d'anneaux de la chenille
     * @param rayon     le rayon des anneaux et de la tête de la chenille
     * @param couleur   la couleur de dessin de la chenille
     */
    public ChenilleCouleur(Dessin dess, int nbAnneaux, int rayon,
            Color couleur, IAnimateur animateur) {
        super(dess, nbAnneaux, rayon, animateur);
        this.couleur = couleur;
    }

    @Override
    public void dessiner(Graphics g) {
        Graphics ctxtGrahique = g.create();// fait une copie de g
        ctxtGrahique.setColor(this.couleur);
        super.dessiner(ctxtGrahique);
    }

    public Color getCouleur() {
        return couleur;
    }

    public void setCouleur(Color couleur) {
        this.couleur = couleur;
    }

}
