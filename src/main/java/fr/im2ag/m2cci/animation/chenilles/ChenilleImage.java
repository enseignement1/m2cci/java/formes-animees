/*
 * Copyright (C) 2022 Philippe GENOUD - Université Grenoble Alpes (UGA) - Laboratoire Informatique de Grenoble (LIG) - équipe STeamer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.im2ag.m2cci.animation.chenilles;

import java.awt.image.BufferedImage;

import fr.im2ag.m2cci.animation.Dessin;
import fr.im2ag.m2cci.animation.IAnimateur;
import fr.im2ag.m2cci.animation.animateurs.AnimateurCap;

/**
 * Chenille dont la tête est défine par une image
 * 
 * @author Philippe GENOUD - Université Grenoble Alpes (UGA) - Laboratoire
 *         Informatique de Grenoble (LIG) - équipe STeamer
 */
public class ChenilleImage extends Chenille {

    /**
     * Chenille dont la tête est une image
     * 
     * @param dess      la feuille de dessin où se situe la chenille
     * @param nbAnneaux nombre d'anneaux de la chenille
     * @param rayon     la rayon de la tête
     * @param img       l'image de la tête
     * @param animateur l'animateur en charge des déplacements de la tête de la
     *                  chenille
     */
    public ChenilleImage(Dessin dess, int nbAnneaux, int rayon, BufferedImage img, IAnimateur animateur) {
        super(nbAnneaux, rayon,
                new TeteImage(img, 0.0, dess.getLargeur() / 2, dess.getHauteur() / 2, rayon),
                animateur);
    }

    /**
     * Chenille dont la tête est une image, l'animateur est par défaut un animateur
     * de type AnimateurCap
     * 
     * @param dess      la feuille de dessin où se situe la chenille
     * @param nbAnneaux nombre d'anneaux de la chenille
     * @param rayon     la rayon de la tête
     * @param img       l'image de la tête
     */
    public ChenilleImage(Dessin dess, int nbAnneaux, int rayon, BufferedImage img) {
        this(dess, nbAnneaux, rayon, img, new AnimateurCap(dess, rayon, 0));
    }

}
