/*
 * Copyright (C) 2022 Philippe GENOUD - Université Grenoble Alpes (UGA) - Laboratoire Informatique de Grenoble (LIG) - équipe STeamer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.im2ag.m2cci.animation.chenilles;

import java.awt.Graphics;

/**
 * Représente la tête d'une chenille. Une Tete est est un anneau particulier qui
 * en plus des attributs d'un anneau possède un cap (direction de déplacement).
 *
 * L'affichage d'une tête est différent de celui d'un anneau. Il s'effectue en
 * traçant un disque noir au lieu d'un simple cercle.
 *
 * @author Philippe GENOUD - Université Grenoble Alpes (UGA) - Laboratoire
 * Informatique de Grenoble (LIG) - équipe STeamer
 */
public class Tete extends Anneau {

    //------- variables d'instance (attributs) --------------------------
    /**
     * donne le cap de la tête, c'est à dire sa direction de déplacement. Angle
     * en degrés par rapport à l'horizontale.
     */
    private double cap;

    //-------- Constructeurs ---------------------------------------------
    /**
     * créée une tête en fixant sa position initiale, son rayon et son cap
     *
     * @param cap le cap initial
     * @param xInit l'abscisse du centre de la tête
     * @param yInit l'ordonnée du centre de la tête
     * @param r le rayon de la tête
     */
    public Tete(double cap, int xInit, int yInit, int r) {
        super(xInit, yInit, r);
        this.cap = cap;
    }

    /**
     * créée une tête en fixant sa position initiale, son rayon est le rayon par
     * défaut défini pour les anneaux, et son cap est nul.
     *
     * @param xInit l'abscisse du centre de la tête
     * @param yInit l'abscisse du centre de la tête
     */
    public Tete(int xInit, int yInit) {
        super(xInit, yInit);
        this.cap = 0.0;
    }

    //-------- Méthodes ---------------------------------------------  
    /**
     * affiche la tête en la matérialisant par un disque noir
     *
     * @param g cet objet de classe Graphics passé en paramètre est l'objet qui
     * prend en charge la gestion de l'affichage dans la fenêtre de dessin.
     * C'est cet objet qui gère le "contexte graphique" pour cette fenêtre.
     */
    @Override
    public void dessiner(Graphics g) {
        g.fillOval(this.x - this.r, this.y - this.r, 2 * this.r, 2 * this.r);
    }

    /**
     * fait effectuer à la tête un déplacement élémentaire d'une distance de r
     * dans la direction définie par son cap
     */
    public void deplacerSelonCap() {
        this.x = (int) (this.x + this.r * Math.cos(Math.toRadians(cap)));
        this.y = (int) (this.y + this.r * Math.sin(Math.toRadians(cap)));
    }

    /**
     * Modifie le cap de la chenille. Le cap courant de la chenille est modifié
     * en lui ajoutant une variation de cap passée en paramètre.
     *
     * @param deltaCap la variation à appliquer au cap de la chenille (en degrés).
     */
    public void devierCap(double deltaCap) {
        this.cap += deltaCap; // this.cap = this.cap + deltaCap
    }

    /**
     * teste si le cap actuel garantit que prochain déplacement de la tête selon
     * son cap maintiendra celle-ci entièrement dans la zone de dessin.
     *
     * @param xMax la taille en x (largeur) de la zône de dessin
     * @param yMax la taille en y (hauteur) de la zône de dessin
     *
     * @return true le point (x',y') défini par x' = x + r * cos(cap) et 
     *         y' = y + r * sin(cap) est à une distance >= R de chacun des
     *         bords de la zone.
     */
    public boolean capOK(int xMax, int yMax) {
        int x1 = (int) (this.x + this.r * Math.cos(Math.toRadians(cap)));
        int y1 = (int) (this.y + this.r * Math.sin(Math.toRadians(cap)));

        return x1 >= this.r && x1 <= xMax - this.r && y1 >= this.r && y1 <= yMax - this.r;
    }

}
