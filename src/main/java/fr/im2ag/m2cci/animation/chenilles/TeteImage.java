/*
 * Copyright (C) 2022 Philippe GENOUD - Université Grenoble Alpes (UGA) - Laboratoire Informatique de Grenoble (LIG) - équipe STeamer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.im2ag.m2cci.animation.chenilles;

import java.awt.Graphics;
import java.awt.image.BufferedImage;

/**
 * Tete de Chenille, dessinée à l'aide d'une image
 *
 * @author Philippe GENOUD - Université Grenoble Alpes (UGA) - Laboratoire
 * Informatique de Grenoble (LIG) - équipe STeamer
 */
public class TeteImage extends Tete {

    final private BufferedImage img;

    /**
     * créée une tête en fixant sa position initiale, son rayon et son cap
     *
     * @param img l'image de la tête
     * @param cap le cap initial
     * @param xInit l'abscisse du centre de la tête
     * @param yInit l'ordonnée du centre de la tête
     * @param r le rayon de la tête
     */
    public TeteImage(BufferedImage img, double cap, int xInit, int yInit, int r) {
        super(cap, xInit, yInit, r);
        this.img = img;
    }

    @Override
    public void dessiner(Graphics g) {
        g.drawImage(this.img, this.x - this.r , this.y -this.r, 
                this.x + this.r, this.y + this.r, 0, 0, img.getWidth(), img.getHeight(), null);
    }

}
