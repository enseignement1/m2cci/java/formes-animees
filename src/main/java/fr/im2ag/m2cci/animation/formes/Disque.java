package fr.im2ag.m2cci.animation.formes;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;

public class Disque extends FormeCirculaire {

    /**
     * constructeur d'un disque
     * 
     * @param x       abscisse du centre du disque
     * @param y       ordonnée du centre du disque
     * @param r       rayon du disque
     * @param epTrait épaisseur du trait de contour du disque
     * @param cTrait  couleur du trait de contour du disque
     * @param cRemp   couleur de remplissage du disque
     */
    public Disque(int x, int y, int r, float epTrait, Color cTrait, Color cRemp) {
        super(x, y, r, epTrait, cTrait, cRemp);
    }

    @Override
    public void dessiner(Graphics g) {
        // dessin à l'aide d'un objet Graphics g
        Graphics2D g2 = (Graphics2D) g.create(); // on crée une copie de g

        // Etape 3
        // dessin du contour
        g2.setColor(this.cTrait);
        g2.setStroke(new BasicStroke(this.epTrait));
        g2.drawOval(this.x - this.r, this.y - this.r, 2 * r, 2 * r);

        // Etape 4
        // Remplissage de la forme
        g2.setPaint(this.cRemp);
        g2.fillOval(this.x - this.r, this.y - this.r, 2 * r, 2 * r);

    }

}
