/*
 * Copyright (C) 2021 Philippe GENOUD - Université Grenoble Alpes (UGA) - Laboratoire Informatique de Grenoble (LIG) - équipe STeamer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.im2ag.m2cci.animation.formes;

import java.awt.Color;
import java.awt.geom.Path2D;
import java.awt.geom.Point2D;

/**
 * Une étoile à 5 branches
 * 
 * @author Philippe GENOUD - Université Grenoble Alpes (UGA) - Laboratoire
 * Informatique de Grenoble (LIG) - équipe STeamer
 */
public class Etoile extends FormeCirculaireReguliere {

    /**
     * Constructeur d'une étoile
     *
     * @param x abscisse du centre de l'étoile
     * @param y ordonnée du centre de l'étoile
     * @param r rayon du cercle dans lequel l'étoile est inscrite
     * @param epTrait épaisseur du trait de contour
     * @param cTrait couleur du trait de contour
     * @param cRemp couleur de remplissage de l'étoile
     */
    public Etoile(int x, int y, int r, float epTrait, Color cTrait, Color cRemp) {
        super(5, x, y, r, epTrait, cTrait, cRemp);
    }

    @Override
    protected Path2D relierSommets(Point2D[] sommets) {
                // Etape 2
        // construction du chemin reliant les points
        Path2D contour = new Path2D.Float();
        contour.moveTo(sommets[0].getX(), sommets[0].getY());
        contour.lineTo(sommets[2].getX(), sommets[2].getY());
        contour.lineTo(sommets[4].getX(), sommets[4].getY());
        contour.lineTo(sommets[1].getX(), sommets[1].getY());
        contour.lineTo(sommets[3].getX(), sommets[3].getY());
        contour.closePath();
        return contour;
    }

}

