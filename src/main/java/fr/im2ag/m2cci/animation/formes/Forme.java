/*
 * Copyright (C) 2022 Philippe GENOUD - Université Grenoble Alpes (UGA) - Laboratoire Informatique de Grenoble (LIG) - équipe STeamer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.im2ag.m2cci.animation.formes;

import java.awt.Color;
import java.awt.Rectangle;

import fr.im2ag.m2cci.animation.IForme;

/**
 * Définit une forme géométrique quelconque.
 * 
 * @author Philippe GENOUD - Université Grenoble Alpes (UGA) - Laboratoire
 * Informatique de Grenoble (LIG) - équipe STeamer
 */
public abstract class Forme implements IForme {

    /**
     * abscisse du point de référence de la forme
     */
    protected int x;
    /**
     * l'ordonnée du point de référence de la forme
     */
    protected int y;

    /**
     * l'épaisseur du trait de contour
     */
    protected float epTrait;

    /**
     * la couleur du trait de contour
     */
    protected Color cTrait;

    /**
     * la couleur de remplissage de la forme
     */
    protected Color cRemp;


    /**
     * Constructeur d'une forme 
     *
     * @param x abscisse du point de référence de la forme
     * @param y ordonnée point de référence de la forme
     * @param epTrait épaisseur du trait de contour
     * @param cTrait couleur du trait de contour
     * @param cRemp couleur de remplissage de la forme
     */
    public Forme(int x, int y, float epTrait, Color cTrait, Color cRemp) {
        this.x = x;
        this.y = y;
        this.epTrait = epTrait;
        this.cTrait = cTrait;
        this.cRemp = cRemp;
    }

    /**
     * permet de modifier la position du point de référence de la forme
     * @param x nouvelle abscisse du point de référence
     * @param y nouvelle ordonnée du point de référence
     */
    public void placerA(int x, int y) {
        this.x = x;
        this.y = y;
    }

    // getters et setters pour modifier les attributs décrivant la forme

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }


    public float getEpTrait() {
        return epTrait;
    }

    public void setEpTrait(float epTrait) {
        this.epTrait = epTrait;
    }

    public Color getcTrait() {
        return cTrait;
    }

    public void setcTrait(Color cTrait) {
        this.cTrait = cTrait;
    }

    public Color getcRemp() {
        return cRemp;
    }

    public void setcRemp(Color cRemp) {
        this.cRemp = cRemp;
    }

    /**
     * calcule le rectangle englobant de la forme
     * @return le réctangle englobant
     */
    public abstract Rectangle getRectEnglobant() ;

}
