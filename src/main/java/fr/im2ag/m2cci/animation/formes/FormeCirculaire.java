/*
 * Copyright (C) 2021 Philippe GENOUD - Université Grenoble Alpes (UGA) - Laboratoire Informatique de Grenoble (LIG) - équipe STeamer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.im2ag.m2cci.animation.formes;

import java.awt.Color;
import java.awt.Rectangle;

/**
 *
 * @author Philippe GENOUD - Université Grenoble Alpes (UGA) - Laboratoire
 * Informatique de Grenoble (LIG) - équipe STeamer
 */
public abstract class FormeCirculaire extends Forme {

    /**
     * rayon du cercle dans lequel la forme est inscrite
     */
    protected final int r;

    /**
     * Constructeur d'une forme circulaire 
     *
     * @param x abscisse du centre de la forme
     * @param y ordonnée du centre de la forme
     * @param r rayon du cercle dans lequel la forme réguliere est inscrite
     * @param epTrait épaisseur du trait de contour
     * @param cTrait couleur du trait de contour
     * @param cRemp couleur de remplissage de la forme
     */
    public FormeCirculaire(int x, int y, int r, float epTrait, Color cTrait, Color cRemp) {
        super(x, y, epTrait, cTrait, cRemp);
        this.r = r;
    }

    public int getR() {
        return r;
    }

    @Override
    public Rectangle getRectEnglobant() {
        return new Rectangle(this.x - this.r , this.y - this.r , 2 * this.r, 2*  this.r);
    }


}

