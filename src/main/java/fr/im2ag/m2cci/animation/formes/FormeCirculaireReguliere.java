/*
 * Copyright (C) 2022 Philippe GENOUD - Université Grenoble Alpes (UGA) - Laboratoire Informatique de Grenoble (LIG) - équipe STeamer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.im2ag.m2cci.animation.formes;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Path2D;
import java.awt.geom.Point2D;

/**
 * Une forme inscrite dans un cercle et dont les sommets sont répartis 
 * uniforméments sur le cercle
 * 
 * @author Philippe GENOUD - Université Grenoble Alpes (UGA) - Laboratoire
 * Informatique de Grenoble (LIG) - équipe STeamer
 */
public abstract class FormeCirculaireReguliere extends FormeCirculaire {

    /**
     * le chemin de contour de la forme
     */
    protected Path2D contour;

    /**
     * Constructeur d'une forme circulaire régulière
     *
     * @param nbSommets le nombre de sommets de la forme
     * @param x abscisse du centre de la forme
     * @param y ordonnée du centre de la forme
     * @param r rayon du cercle dans lequel la forme réguliere est inscrite
     * @param epTrait épaisseur du trait de contour
     * @param cTrait couleur du trait de contour
     * @param cRemp couleur de remplissage de la forme
     */
    public FormeCirculaireReguliere(int nbSommets, int x, int y, int r, float epTrait, Color cTrait, Color cRemp) {
        super(x, y, r, epTrait, cTrait, cRemp);
        contour = construirePath(nbSommets);
    }
    
    /**
     * construit le contour de la forme
     * @param nbSommets le nombre de sommets de la forme
     * @return le contour de la forme
     */
    private Path2D construirePath(int nbSommets) {
        // Etape 1
        // calcul des sommets de la forme
        float deltaAngle = 360f / nbSommets;
        float angle = -90;
        Point2D.Float[] sommets = new Point2D.Float[nbSommets];
        for (int i = 0; i < nbSommets; i++) {
            sommets[i] = new Point2D.Float((float) Math.cos(Math.toRadians(angle)) * r,
                    (float) Math.sin(Math.toRadians(angle)) * r);
            angle += deltaAngle;
        }

        return relierSommets(sommets);
    }
    
    /**
     * construit le contour en reliant les somets de la forme circulaire régulière
     * @param sommets le tableau des sommets répartis uniformément sur le cercle.
     * correspond à l'étape 2
     * @return le contour reliant les sommets
     */
    protected abstract Path2D relierSommets(Point2D[] sommets);

    @Override
    public void dessiner(Graphics g) {
        // dessin à l'aide d'un objet Graphics g
        Graphics2D g2 = (Graphics2D) g.create();   // on crée une copie de g

       // Etape 3
        // dessin du contour
        g2.setColor(this.cTrait);
        g2.setStroke(new BasicStroke(this.epTrait));
        g2.translate(x, y);  // x et y le centre du cercle définissant le polygone régulier
        g2.draw(this.contour);

        // Etape 4
        // Remplissage de la forme
        g2.setPaint(this.cRemp);
        g2.fill(this.contour);
    }

}

