/*
 * Copyright (C) 2022 Philippe GENOUD - Université Grenoble Alpes (UGA) - Laboratoire Informatique de Grenoble (LIG) - équipe STeamer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.im2ag.m2cci.animation.formes;

import java.awt.Color;
import java.awt.geom.Path2D;
import java.awt.geom.Point2D;

/**
 * Un Polygone régulier
 *
 * @author Philippe GENOUD - Université Grenoble Alpes (UGA) - Laboratoire
 * Informatique de Grenoble (LIG) - équipe STeamer
 */
public class PolygoneRegulier extends FormeCirculaireReguliere {

    /**
     * Constructeur d'un PolygoneRegulier
     *
     * @param nbSommets le nombre de sommets du polygone régulier
     * @param x abscisse du centre du polygone régulier
     * @param y ordonnée du centre du polygone régulier
     * @param r rayon du cercle dans lequel le polygone régulier est inscrite
     * @param epTrait épaisseur du trait de contour
     * @param cTrait couleur du trait de contour
     * @param cRemp couleur de remplissage du polygone régulier
     */
    public PolygoneRegulier(int nbSommets, int x, int y, int r, float epTrait, Color cTrait, Color cRemp) {
        super(nbSommets, x, y, r, epTrait, cTrait, cRemp);
    }

    @Override
    protected Path2D relierSommets(Point2D[] sommets) {
        // Etape 2
        // construction du chemin reliant les points
        Path2D contour = new Path2D.Float();
        contour.moveTo(sommets[0].getX(), sommets[0].getY());
        for (int i = 1; i < sommets.length; i++) {
            contour.lineTo(sommets[i].getX(), sommets[i].getY());
        }
        contour.closePath();
        return contour;
    }

}

